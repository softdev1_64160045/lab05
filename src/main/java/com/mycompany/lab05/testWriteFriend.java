/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab05;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chana
 */
public class testWriteFriend {

    public static void main(String[] args)  {
        FileOutputStream fos = null;
        try {
            Friend f1 = new Friend("Bom", 10, "0147852369");
            Friend f2 = new Friend("Bam", 20, "0123654789");
            System.out.println(f1);
            System.out.println(f2);
            File file = new File("friends.dat");
            fos = new  FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f1);
            oos.writeObject(f2);
            oos.close();
            fos.close();
        } catch (Exception ex) {
            System.out.println("Error");
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(testWriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
